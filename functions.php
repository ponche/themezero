<?php 
function display_hello()
{
    echo 'Bonjour les zéros'; 
}

// ajout d'un nouvelle emplacement de widget 
add_action('widgets_init', 'zero_add_sidebar'); 
function zero_add_sidebar()
{
    register_sidebar(array(
        'id' => 'my_custom_zone', 
        'name' => 'Zone supérieur', 
        'description' => 'Apparait en haut du site', 
));
}

// ajout d'un menu 
add_action('init', 'zero_add_menu'); 
function zero_add_menu()
{
    register_nav_menu('main_menu', 'Menu principal') ; 
}

// test d'un filtre 
add_filter('the_title', 'truncate_long_title');
function truncate_long_title($title)
{
    if (strlen($title) > 3 ) 
    {
        $title = substr($title, 0, 3).'...' ; 
    }
    return $title ; 
}

// ajouter des template personaliser 
get_template_part('mon_template.php') ; 

