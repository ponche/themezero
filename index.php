
<?php get_header() ?>

<?php 
display_hello() ; 
?>

// creation de l'emplacement widget 
dynamic_sidebar('my_custom_zone') ; 

<?php
// appel d'un widget sans passer par une zone , le calandrier 
the_widget('WP_Widget_Calendar') ; 
?>

<h2>liste des article</h2>
<?php
while (have_posts()) : 
    the_post();
    the_title() ; 
    the_content();
endwhile; 
?>

<?php get_footer() ?> 

